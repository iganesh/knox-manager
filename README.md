# Knox Manager

## Instructions 

### Prerequisites

You'll need a Knox Platform for Enterprise (KPE) Development license key in order to use the Knox Manager. <br/>
You'll need to enroll as a developer in order to get this license. As for the developer, the license needs to be renewed every 3 months interval.<br/>
When you receive a mail about license expiration, you usually cannot generate a new key immediately and you'll need to wait for sometime.</br>

- Samsung S series phone, tested on S8, S8+, S9 and S9+.
- Enroll as a developer with this link: https://seap.samsung.com/enrollment
- Generate a license key with this link: https://seap.samsung.com/license-keys/create#section-knox-sdk
- Give an alias name, e.g. test
- Click on `Generate License Key`
- There will be two keys: KPE key and backwards-compatible key<br/>

### Java
- Install JDK 8 for your platform http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
 
### Git (Optional, but recommended)
- Install git for your platform: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

### Source code
- Using git: Clone the project with `git clone https://gitlab.com/iganesh/knox-manager.git`

### Android Studio
- Download and install latest Android Studio from https://developer.android.com/studio/index.html
- Open the Knox-Manager project in Android Studio
- Install the missing SDK, build-tools and other things that are prompted

### Knox SDK
- Download latest Knox SDK and supportlib from https://seap.samsung.com/sdk/knox-android
- Create `libs` sub-folder in `app` folder: `app\libs`
- Extract `knoxsdk.jar` and `supportlib.jar` respectively from both zip files
- Rename the supportlib jar file name to `supportlib.jar`
- Put both jar files to `app\libs` folder

# Credits

## Icons
Logo from [Flaticon](https://www.flaticon.com/free-icon/security_1308823#term=firewall&page=1&position=33) <br/>
UI Icons from [Material Icons](https://material.io/icons)

# Disclaimer
Knox Manager is merely an app that is using the Samsung Knox SDK APIs. <br/>
In order to use these APIs, the Knox SDK and a KPE Development license key are needed. <br/>
These are Samsung's properties which are not available in this repository and therefore they need to be downloaded and obtained by the developer after accepting the agreement given by Samsung. <br/>
The developer is then responsible how this app will be used and I don't take any responsibilities of any damages caused by this app. <br/>

The Knox SDK can be downloaded here: https://seap.samsung.com/sdk/knox-android <br/>
The Knox License key can be obtained here: https://seap.samsung.com/license-keys/create#section-knox-sdk <br/>
The API can be found here: https://seap.samsung.com/api-references/android/reference/packages.html
