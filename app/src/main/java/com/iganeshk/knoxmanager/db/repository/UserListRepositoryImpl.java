package com.iganeshk.knoxmanager.db.repository;

import com.iganeshk.knoxmanager.db.AppDatabase;
import com.iganeshk.knoxmanager.utils.AppFactory;

import io.reactivex.Single;

public abstract class UserListRepositoryImpl implements UserListRepository {

    protected AppDatabase appDatabase;

    UserListRepositoryImpl() {
        this.appDatabase = AppFactory.getInstance().getAppDatabase();
    }

    @Override
    public Single<String> addItem(String item) {
        if (item == null || item.isEmpty()) {
            return Single.error(new IllegalArgumentException("Item cannot be null or empty"));
        }
        return Single.create(emitter -> {
            addItemToDatabase(item);
            emitter.onSuccess(item);
        });
    }

    @Override
    public Single<String> removeItem(String item) {
        if (item == null || item.isEmpty()) {
            return Single.error(new IllegalArgumentException("Item cannot be null or empty"));
        }
        return Single.create(emitter -> {
            removeItemFromDatabase(item);
            emitter.onSuccess(item);
        });
    }
}
