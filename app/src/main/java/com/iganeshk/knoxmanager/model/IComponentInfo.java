package com.iganeshk.knoxmanager.model;

public interface IComponentInfo {

    String getPackageName();

}
