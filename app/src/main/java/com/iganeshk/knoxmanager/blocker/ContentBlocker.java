package com.iganeshk.knoxmanager.blocker;

import android.os.Handler;

public interface ContentBlocker {
    void enableDomainRules(boolean updateProviders);
    void disableDomainRules();
    void enableFirewallRules();
    void disableFirewallRules();
    boolean isEnabled();
    boolean isDomainRuleEmpty();
    boolean isFirewallRuleEmpty();
    void setHandler(Handler handler);
}
