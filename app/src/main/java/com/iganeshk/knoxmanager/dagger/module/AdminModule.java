package com.iganeshk.knoxmanager.dagger.module;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;

import com.iganeshk.knoxmanager.dagger.scope.KnoxManagerApplicationScope;
import com.iganeshk.knoxmanager.receiver.CustomDeviceAdminReceiver;

import dagger.Module;
import dagger.Provides;

@Module(includes = {AppModule.class})
public class AdminModule {
    @Provides
    @KnoxManagerApplicationScope
    DevicePolicyManager providesDevicePolicyManager(Context appContext) {
        return (DevicePolicyManager) appContext.getSystemService(Context.DEVICE_POLICY_SERVICE);
    }

    @Provides
    @KnoxManagerApplicationScope
    ComponentName providesComponentName(Context appContext) {
        return new ComponentName(appContext, CustomDeviceAdminReceiver.class);
    }
}
