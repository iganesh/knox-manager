package com.iganeshk.knoxmanager.dagger.component;

import com.iganeshk.knoxmanager.dagger.module.AdminModule;
import com.iganeshk.knoxmanager.dagger.module.AppModule;
import com.iganeshk.knoxmanager.dagger.module.EnterpriseModule;
import com.iganeshk.knoxmanager.dagger.scope.KnoxManagerApplicationScope;
import com.iganeshk.knoxmanager.utils.AppFactory;
import com.iganeshk.knoxmanager.utils.DeviceAdminInteractor;

import dagger.Component;

@KnoxManagerApplicationScope
@Component(modules = {AppModule.class, AdminModule.class, EnterpriseModule.class})
public interface AppComponent {
    void inject(DeviceAdminInteractor deviceAdminInteractor);
    void inject(AppFactory appFactory);
}
