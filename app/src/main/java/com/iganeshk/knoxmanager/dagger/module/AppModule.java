package com.iganeshk.knoxmanager.dagger.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import com.iganeshk.knoxmanager.dagger.scope.KnoxManagerApplicationScope;
import com.iganeshk.knoxmanager.db.AppDatabase;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private static final String APP_GENERAL_PREFERENCES = "app_general_preferences";

    private Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @KnoxManagerApplicationScope
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @KnoxManagerApplicationScope
    Context providesContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    @KnoxManagerApplicationScope
    AppDatabase providesAppDatabase() {
        return AppDatabase.getAppDatabase(mApplication.getApplicationContext());
    }

    @Provides
    @KnoxManagerApplicationScope
    PackageManager providesPackageManager() {
        return mApplication.getPackageManager();
    }

    @Provides
    @KnoxManagerApplicationScope
    SharedPreferences providesSharedPreferences(Context context) {
        return context.getSharedPreferences(APP_GENERAL_PREFERENCES, Context.MODE_PRIVATE);
    }
}
