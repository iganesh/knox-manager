package com.iganeshk.knoxmanager.adapter;

import android.content.ComponentName;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.iganeshk.knoxmanager.R;
import com.iganeshk.knoxmanager.model.IComponentInfo;
import com.iganeshk.knoxmanager.model.ServiceInfo;
import com.iganeshk.knoxmanager.utils.AppFactory;
import com.iganeshk.knoxmanager.utils.AppPreferences;
import com.samsung.android.knox.application.ApplicationPolicy;

import java.util.List;

public class ServiceInfoAdapter extends ComponentAdapter {

    public ServiceInfoAdapter(@NonNull Context context, @NonNull List<IComponentInfo> componentInfos) {
        super(context, componentInfos);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_service_info, parent, false);
        }

        IComponentInfo componentInfo = getItem(position);
        if (componentInfo == null) {
            return convertView;
        }

        if (componentInfo instanceof ServiceInfo) {
            ServiceInfo serviceInfo = (ServiceInfo) componentInfo;
            String packageName = serviceInfo.getPackageName();
            String serviceName = serviceInfo.getName();
            TextView serviceNameTextView = convertView.findViewById(R.id.serviceNameTextView);
            Switch permissionSwitch = convertView.findViewById(R.id.switchDisable);
            serviceNameTextView.setText(serviceName);
            permissionSwitch.setChecked(getComponentState(packageName, serviceName));

            boolean enabled = AppPreferences.getInstance().isAppComponentToggleEnabled();
            permissionSwitch.setEnabled(enabled);
        }

        return convertView;
    }

    private boolean getComponentState(String packageName, String serviceName) {
        ApplicationPolicy appPolicy = AppFactory.getInstance().getAppPolicy();
        if (appPolicy == null) {
            return false;
        }

        ComponentName componentName = new ComponentName(packageName, serviceName);
        return appPolicy.getApplicationComponentState(componentName);
    }
}
