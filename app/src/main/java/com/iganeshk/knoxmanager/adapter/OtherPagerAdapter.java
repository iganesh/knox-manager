package com.iganeshk.knoxmanager.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.iganeshk.knoxmanager.BuildConfig;
import com.iganeshk.knoxmanager.R;
import com.iganeshk.knoxmanager.fragments.OtherTabPageFragment;

public class OtherPagerAdapter extends FragmentPagerAdapter {
    private static final int PAGE_COUNT = 2;
//    private static final int PAGE_COUNT = 3;
    private String tabTitles[];

    public OtherPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabTitles = new String[] {
                context.getString(R.string.app_component_fragment_title),
                context.getString(R.string.dns_fragment_title),
                context.getString(R.string.settings_fragment_title)
        };
    }

    @Override
    public Fragment getItem(int position) {
        return OtherTabPageFragment.newInstance(BuildConfig.APP_COMPONENT ? position : position + 2);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return BuildConfig.APP_COMPONENT ? PAGE_COUNT : PAGE_COUNT - 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[BuildConfig.APP_COMPONENT ? position : position + 2];
    }
}
