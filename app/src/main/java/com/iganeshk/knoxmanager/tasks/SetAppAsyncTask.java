package com.iganeshk.knoxmanager.tasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import com.iganeshk.knoxmanager.adapter.AppInfoAdapter;
import com.iganeshk.knoxmanager.db.AppDatabase;
import com.iganeshk.knoxmanager.db.DatabaseFactory;
import com.iganeshk.knoxmanager.db.entity.AppInfo;
import com.iganeshk.knoxmanager.db.entity.DisabledPackage;
import com.iganeshk.knoxmanager.db.entity.DnsPackage;
import com.iganeshk.knoxmanager.db.entity.FirewallWhitelistedPackage;
import com.iganeshk.knoxmanager.db.entity.RestrictedPackage;
import com.iganeshk.knoxmanager.model.AppFlag;
import com.iganeshk.knoxmanager.utils.AppIntegrity;
import com.iganeshk.knoxmanager.utils.AppFactory;
import com.samsung.android.knox.application.ApplicationPolicy;

import java.lang.ref.WeakReference;

public class SetAppAsyncTask extends AsyncTask<Void, Void, Void> {
    private AppFlag appFlag;
    private AppInfo appInfo;
    private WeakReference<Context> contextWeakReference;

    public SetAppAsyncTask(AppInfo appInfo, AppFlag appFlag, Context context) {
        this.appInfo = appInfo;
        this.appFlag = appFlag;
        this.contextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        ApplicationPolicy appPolicy = AppFactory.getInstance().getAppPolicy();
        if (appPolicy == null) {
            return null;
        }

        AppDatabase appDatabase = AppFactory.getInstance().getAppDatabase();
        String packageName = appInfo.packageName;
        switch (appFlag.getFlag()) {
            case DISABLER_FLAG:
                appInfo.disabled = !appInfo.disabled;
                if (appInfo.disabled) {
                    DisabledPackage disabledPackage = new DisabledPackage();
                    disabledPackage.packageName = packageName;
                    disabledPackage.policyPackageId = AppIntegrity.DEFAULT_POLICY_ID;
                    appDatabase.disabledPackageDao().insert(disabledPackage);
                } else {
                    appDatabase.disabledPackageDao().deleteByPackageName(packageName);
                }

                if (appInfo.disabled) {
                    appPolicy.setDisableApplication(packageName);
                } else {
                    appPolicy.setEnableApplication(packageName);
                }
                break;

            case MOBILE_RESTRICTED_FLAG:
                appInfo.mobileRestricted = !appInfo.mobileRestricted;
                if (appInfo.mobileRestricted) {
                    RestrictedPackage restrictedPackage = new RestrictedPackage();
                    restrictedPackage.packageName = packageName;
                    restrictedPackage.type = DatabaseFactory.MOBILE_RESTRICTED_TYPE;
                    restrictedPackage.policyPackageId = AppIntegrity.DEFAULT_POLICY_ID;
                    appDatabase.restrictedPackageDao().insert(restrictedPackage);
                } else {
                    appDatabase.restrictedPackageDao().deleteByPackageName(packageName, DatabaseFactory.MOBILE_RESTRICTED_TYPE);
                }
                break;

            case WIFI_RESTRICTED_FLAG:
                appInfo.wifiRestricted = !appInfo.wifiRestricted;
                if (appInfo.wifiRestricted) {
                    RestrictedPackage restrictedPackage = new RestrictedPackage();
                    restrictedPackage.packageName = packageName;
                    restrictedPackage.type = DatabaseFactory.WIFI_RESTRICTED_TYPE;
                    restrictedPackage.policyPackageId = AppIntegrity.DEFAULT_POLICY_ID;
                    appDatabase.restrictedPackageDao().insert(restrictedPackage);
                } else {
                    appDatabase.restrictedPackageDao().deleteByPackageName(packageName, DatabaseFactory.WIFI_RESTRICTED_TYPE);
                }
                break;

            case WHITELISTED_FLAG:
                appInfo.knoxmanagerWhitelisted = !appInfo.knoxmanagerWhitelisted;
                if (appInfo.knoxmanagerWhitelisted) {
                    FirewallWhitelistedPackage whitelistedPackage = new FirewallWhitelistedPackage();
                    whitelistedPackage.packageName = packageName;
                    whitelistedPackage.policyPackageId = AppIntegrity.DEFAULT_POLICY_ID;
                    appDatabase.firewallWhitelistedPackageDao().insert(whitelistedPackage);
                } else {
                    appDatabase.firewallWhitelistedPackageDao().deleteByPackageName(packageName);
                }
                break;

            case DNS_FLAG:
                appInfo.hasCustomDns = !appInfo.hasCustomDns;
                if (appInfo.hasCustomDns) {
                    DnsPackage dnsPackage = new DnsPackage();
                    dnsPackage.packageName = packageName;
                    dnsPackage.policyPackageId = AppIntegrity.DEFAULT_POLICY_ID;
                    appDatabase.dnsPackageDao().insert(dnsPackage);
                } else {
                    appDatabase.dnsPackageDao().deleteByPackageName(packageName);
                }
                break;
        }
        appDatabase.applicationInfoDao().update(appInfo);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Context context = contextWeakReference.get();
        if (context != null) {
            ListView listView = ((Activity) context).findViewById(appFlag.getLoadLayout());
            if (listView != null) {
                if (listView.getAdapter() instanceof AppInfoAdapter) {
                    ((AppInfoAdapter) listView.getAdapter()).notifyDataSetChanged();
                }
            }
        }
    }
}
