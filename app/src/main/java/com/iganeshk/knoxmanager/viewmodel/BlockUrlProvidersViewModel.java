package com.iganeshk.knoxmanager.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.iganeshk.knoxmanager.db.AppDatabase;
import com.iganeshk.knoxmanager.db.entity.BlockUrlProvider;
import com.iganeshk.knoxmanager.utils.AppFactory;

import java.util.List;

public class BlockUrlProvidersViewModel extends ViewModel {
    private LiveData<List<BlockUrlProvider>> blockUrlProviders;

    public BlockUrlProvidersViewModel() {
    }

    public LiveData<List<BlockUrlProvider>> getBlockUrlProviders() {
        if (blockUrlProviders == null) {
            blockUrlProviders = new MutableLiveData<>();
            loadBlockUrlProviders();
        }
        return blockUrlProviders;
    }

    private void loadBlockUrlProviders() {
        AppDatabase appDatabase = AppFactory.getInstance().getAppDatabase();
        blockUrlProviders = appDatabase.blockUrlProviderDao().getAll();
    }
}
